const mysql = require('mysql');
const { promisify } = require('util');
const { database } = require('./keys');

const pool = mysql.createPool(database);


pool.getConnection((err,connection) => {
    if(err){
        if(err.code === 'PROTOCOL_CONNECTION_LOST'){
            console.error('Conexion fue cerrada');
        }
        if (err.code === 'ER_CON_COUNT_ERROR') {
            console.error('Muchas conexiones'); 
        }
        if (err.code === 'ECONNREFUSED') {
            console.error('Conexion fue rechazada'); 
        }
    }
    if (connection) connection.release();
        console.log('DB conectada');
    return;
});
//promisify pool
pool.query = promisify(pool.query);

module.exports = pool;