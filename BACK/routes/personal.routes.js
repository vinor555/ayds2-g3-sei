const express = require('express');
const router = express.Router();
const personalController = require('../controllers/personal.controller');

router.get('/',personalController.getPersonal);
router.post('/add',personalController.agregarPersonal);
router.get('/id:',personalController.getPersona);



module.exports = router;