-- CREACIÓN DE BASE DE DATOS
CREATE DATABASE ayds2_g3_sei;
USE ayds2_g3_sei;

-- CREACIÓN DE TABLAS
CREATE TABLE tipo (
    id          INT AUTO_INCREMENT PRIMARY KEY,
    nombre      VARCHAR(50) NOT NULL
);

CREATE TABLE curso (
    id          INT AUTO_INCREMENT PRIMARY KEY,
    nombre      VARCHAR(50) NOT NULL
);

CREATE TABLE institucion (
    id          INT AUTO_INCREMENT PRIMARY KEY,
    nombre      VARCHAR(50) NOT NULL,
    direccion   VARCHAR(50) NOT NULL
);

CREATE TABLE personal (
    id          INT AUTO_INCREMENT PRIMARY KEY,
    nombre      VARCHAR(50) NOT NULL,
    apellido    VARCHAR(50) NOT NULL,
    usuario     VARCHAR(15) UNIQUE NOT NULL,
    correo      VARCHAR(50) NOT NULL,
    id_tipo     INT NOT NULL, 
    id_curso    INT NOT NULL,
    id_inst     INT NOT NULL,
    FOREIGN KEY (id_tipo) REFERENCES tipo(id),
    FOREIGN KEY (id_curso) REFERENCES curso(id),
    FOREIGN KEY (id_inst) REFERENCES institucion(id)
);

CREATE TABLE grado (
    id          INT AUTO_INCREMENT PRIMARY KEY,
    nombre      VARCHAR(50) NOT NULL,
    seccion     VARCHAR(5) NOT NULL
);

CREATE TABLE profesor_grado (
    id_personal INT NOT NULL,
    id_grado    INT NOT NULL,
    FOREIGN KEY (id_personal) REFERENCES persnoal(id),
    FOREIGN KEY (id_grado) REFERENCES grado(id)
);

CREATE TABLE padre (
    id          INT AUTO_INCREMENT PRIMARY KEY,
    nombre      VARCHAR(50) NOT NULL,
    correo      VARCHAR(50) NOT NULL
);

CREATE TABLE alumno (
    id          INT AUTO_INCREMENT PRIMARY KEY,
    nombre      VARCHAR(50) NOT NULL,
    correo      VARCHAR(50) NOT NULL,
    id_padre    INT NOT NULL,
    id_grado    INT NOT NULL,
    FOREIGN KEY (id_padre) REFERENCES padre(id),
    FOREIGN KEY (id_grado) REFERENCES grado(id)
);

CREATE TABLE asistencia (
    id          INT AUTO_INCREMENT PRIMARY KEY,
    razon       VARCHAR(100) NOT NULL,
    id_alumno   INT NOT NULL,
    FOREIGN KEY (id_alumno) REFERENCES alumno(id),
);

CREATE TABLE nota (
    id_alumno   INT NOT NULL,
    id_curso    INT NOT NULL,
    nota_final  INT,
    FOREIGN KEY (id_alumno) REFERENCES alumno(id),
    FOREIGN KEY (id_curso) REFERENCES curso(id)
);

CREATE TABLE asignacion (
    id          INT AUTO_INCREMENT NOT NULL,
    id_alumno   INT NOT NULL,
    id_curso    INT NOT NULL,
    FOREIGN KEY (id_alumno) REFERENCES alumno(id),
    FOREIGN KEY (id_curso) REFERENCES curso(id)
);